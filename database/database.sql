CREATE TABLE account_types
(
    id   BIGSERIAL    NOT NULL PRIMARY KEY,
    name varchar(120) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    id              BIGSERIAL    NOT NULL PRIMARY KEY,
    account_type_id BIGINT       NOT NULL,
    email           VARCHAR(255) NOT NULL UNIQUE,
    password        CHAR(97)     NOT NULL,
--     foreign_id   BIGINT       NOT NULL,
    created_at      TIMESTAMPTZ  NOT NULL DEFAULT now(),
    updated_at      TIMESTAMPTZ,
    deleted_at      TIMESTAMPTZ,
    soft_delete     BOOL                  DEFAULT FALSE
);

DROP TABLE IF EXISTS admins;
CREATE TABLE admins
(
    id         BIGSERIAL    NOT NULL PRIMARY KEY REFERENCES accounts (id),
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    last_login TIMESTAMPTZ
);

DROP TABLE IF EXISTS students;
CREATE TABLE students
(
    id            BIGSERIAL     NOT NULL PRIMARY KEY REFERENCES accounts (id),
    student_id    CHAR(8)       NOT NULL,
    first_name    VARCHAR(50)   NOT NULL,
    last_name     VARCHAR(50)   NOT NULL,
    phone_number  CHAR(13) UNIQUE,
    national_code CHAR(10),
    gender        VARCHAR(10)   NOT NULL,
    interests     VARCHAR(50)[] NOT NULL DEFAULT '{}',
    expertise     VARCHAR(50)[] NOT NULL DEFAULT '{}',
    major         VARCHAR(50)   NOT NULL,
    education     VARCHAR(50)   NOT NULL
);

DROP TABLE IF EXISTS professors;
CREATE TABLE professors
(
    id            BIGSERIAL   NOT NULL PRIMARY KEY REFERENCES accounts (id),
    first_name    VARCHAR(50) NOT NULL,
    last_name     VARCHAR(50) NOT NULL,
    national_code CHAR(10),
    phone_number  CHAR(13) UNIQUE
);

DROP TABLE IF EXISTS companies;
CREATE TABLE companies
(
    id                  BIGSERIAL    NOT NULL PRIMARY KEY REFERENCES accounts (id),
    registration_code   VARCHAR(255),
    concessionaire_name VARCHAR(255),
    phone_number        CHAR(13),
    state               VARCHAR(255),
    city                VARCHAR(255),
    NAME                VARCHAR(100) NOT NULL,
    address             VARCHAR(255) NOT NULL,
    postal_code         CHAR(10)     NOT NULL UNIQUE
);

-------------------------------------------------------------------
-------------------------------------------------------------------

DROP TABLE IF EXISTS jobs;
CREATE TABLE jobs
(
    id           BIGSERIAL     NOT NULL PRIMARY KEY,
    description  TEXT          NOT NULL,
    requirements VARCHAR(50)[] NOT NULL DEFAULT '{}',
    tags         VARCHAR(50)[] NOT NULL DEFAULT '{}',
    employer_id  BIGINT        NOT NULL REFERENCES accounts (id), -- owner of the job
    created_at   TIMESTAMPTZ   NOT NULL DEFAULT now(),
    updated_at   TIMESTAMPTZ,
    deleted_at   TIMESTAMPTZ,
    soft_delete  BOOL                   DEFAULT FALSE
);

DROP TABLE IF EXISTS job_employee;
CREATE TABLE job_employee
(
    job_id     BIGINT NOT NULL REFERENCES jobs (id),
    account_id BIGINT NOT NULL REFERENCES accounts (id),
    status     BOOL   NOT NULL DEFAULT FALSE
);

DROP TABLE IF EXISTS tags;
CREATE TABLE tags
(
    id   BIGSERIAL   NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE requests
(
    id          BIGSERIAL NOT NULL PRIMARY KEY,
    account_id  BIGINT REFERENCES accounts (id),
    job_id      BIGINT REFERENCES jobs (id),
    price       FLOAT     NOT NULL,
    timespan    INT       NOT NULL,
    description TEXT      NOT NULL,
    status      BOOL DEFAULT FALSE,
    soft_delete BOOL DEFAULT FALSE
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments
(
    id          BIGSERIAL   NOT NULL PRIMARY KEY,
    description TEXT        NOT NULL,
    score       FLOAT,
    job_id      BIGINT      NOT NULL REFERENCES jobs (id),
    account_id  BIGINT      NOT NULL REFERENCES accounts (id),
    created_at  TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at  TIMESTAMPTZ

);

DROP TABLE IF EXISTS job_tag;
CREATE TABLE job_tag
(
    job_id BIGINT NOT NULL REFERENCES jobs (id),
    tag_id BIGINT NOT NULL REFERENCES tags (id)
);

-------------------------------------------------------------------
-------------------------------------------------------------------

INSERT INTO account_types(name)
VALUES ('admin');
INSERT INTO account_types(name)
VALUES ('student');
INSERT INTO account_types(name)
VALUES ('company');
INSERT INTO account_types(name)
VALUES ('professor');


INSERT INTO accounts(account_type, email, password)
VALUES (1, 'mohammad.shoaei@outlook.com',
        '$argon2id$v=19$m=32768,t=4,p=1$d2L3yJWec36NjFvpgTpxrA$SlHFZoQaALF3LbkkcRXHEqJGFjBBfCw5MpXU87rZIh8');

INSERT INTO admins
VALUES (1, 'Mohammad', 'Shoaei', NULL)