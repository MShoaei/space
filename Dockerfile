FROM golang:1.12-alpine
RUN mkdir -p /go/src/github.com/MShoaei/Space
WORKDIR /go/src/github.com/MShoaei/Space
COPY . .
RUN go install . && chmod +x /go/bin/Space
EXPOSE 8085
ENTRYPOINT ["Space"]