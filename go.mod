module github.com/MShoaei/Space

go 1.13

require (
	github.com/CloudyKit/fastprinter v0.0.0-20170127035650-74b38d55f37a // indirect
	github.com/ajg/form v1.5.1 // indirect
	github.com/alexedwards/argon2id v0.0.0-20190612080829-01a59b2b8802
	github.com/fasthttp-contrib/websocket v0.0.0-20160511215533-1f3b11f56072 // indirect
	github.com/gbrlsnchs/jwt/v3 v3.0.0-beta.1
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris v0.0.0-20190724165155-caf42cfef62b
	github.com/klauspost/compress v1.7.4 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190710185942-9d28bd7c0945 // indirect
	github.com/valyala/fasthttp v1.4.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	google.golang.org/appengine v1.6.1 // indirect
)
