package models

import "time"

type Model struct {
	ID uint32
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  time.Time
}