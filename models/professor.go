package models

// Professor is a representation of a professor
type Professor struct {
	Model
	FirstName  string
	LastName   string
	Email      string
	Password   string
	EmployerID uint32
}
