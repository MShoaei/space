package models

// Student is a representation of a student
type Student struct {
	Model
	StudentID  string
	FirstName  string
	LastName   string
	Gender     string
	Education  string
	Major      string // Name of Majors for postgraduate and undergraduate differ
	Interest   []string
	Expertise  []string
	Password   string
	EmployeeID uint32
}
