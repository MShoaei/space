package models

//Job is a representation of a job
type Job struct {
	Model
	Description  string
	Requirements []string
	Tags         []string
	EmployerID   uint32
}
