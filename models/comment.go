package models

type Comment struct {
	Model
	Text      string
	Parent    uint32
}
