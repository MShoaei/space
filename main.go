package main

import (
	"github.com/MShoaei/Space/api"
	"github.com/kataras/iris"
	_ "github.com/lib/pq"
)

func main() {
	app := api.App()
	app.Run(iris.Addr(":8085"))
}
