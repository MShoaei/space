package api

import (
	"github.com/alexedwards/argon2id"
	"github.com/kataras/iris"
)

var argon2 *argon2id.Params
var app *iris.Application

func init() {
	argon2 = &argon2id.Params{
		Memory:      1 << 15,
		Iterations:  4,
		Parallelism: 1,
		SaltLength:  16,
		KeyLength:   32,
	}
}

func App() *iris.Application {
	app := iris.Default()
	app.Get("/", indexGet)

	app.Post("/login", handleAuthLogin)
	app.Post("/logout", handleAuthLogout)

	adminParty := app.Party("/admin")
	{
		adminParty.Post("/login", handleAdminPost)
	}
	return app
}
