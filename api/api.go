package api

import (
	"time"

	"github.com/MShoaei/Space/models"
	"github.com/alexedwards/argon2id"
	"github.com/gbrlsnchs/jwt/v3"
	"github.com/kataras/iris"
)

var hs = jwt.NewHS256([]byte("RandomSecretKey"))

func indexGet(ctx iris.Context) {
	_, _ = ctx.JSON(iris.Map{
		"Message": "Hello from index",
	})
}

func handleAuthLogin(ctx iris.Context) {
	email := ctx.PostValue("email")
	password := ctx.PostValue("password")
	userType := ctx.PostValue("type")
	var id string
	var passwd string

	// language=PostgreSQL
	row := models.DB.QueryRowx("SELECT id password FROM accounts WHERE email=$1", email)
	if err := row.Scan(&id, &passwd); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		_, _ = ctx.JSON(iris.Map{
			"error": err,
		})
	}
	switch userType {
	case "admin":
	case "professor":
	case "company":
	case "student":
	default:

	}

	if passwd != password {
		ctx.StatusCode(iris.StatusUnauthorized)
		_, _ = ctx.JSON(iris.Map{
			"error": "Incorrect password",
		})
	}
	now := time.Now()
	pl := jwt.Payload{
		ExpirationTime: jwt.NumericDate(now.Add(time.Hour * 24 * 7)),
		IssuedAt:       jwt.NumericDate(now),
	}
	token, _ := jwt.Sign(pl, hs)
	_, _ = ctx.JSON(iris.Map{
		"token":      token,
		"successful": true,
	})
}

func handleAdminPost(ctx iris.Context) {
	email := ctx.PostValue("email")
	password := ctx.PostValue("password")
	var id string
	var passwd string
	row := models.DB.QueryRowx("")
	if err := row.Scan(&id, &passwd); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		_, _ = ctx.JSON(iris.Map{
			"error": err,
		})
	}
	chk, _ := argon2id.ComparePasswordAndHash(password, passwd) // what could go wrong!
	if !chk {
		ctx.StatusCode(iris.StatusUnauthorized)
		_, _ = ctx.JSON(iris.Map{
			"error": "Incorrect password",
		})
		return
	}
	now := time.Now()
	pl := jwt.Payload{
		ExpirationTime: jwt.NumericDate(now.Add(time.Hour * 24 * 7)),
		IssuedAt:       jwt.NumericDate(now),
	}
	token, _ := jwt.Sign(pl, hs)
	_, _ = ctx.JSON(iris.Map{
		"token":      token,
		"successful": true,
	})
}

func handleAuthLogout(ctx iris.Context) {

}

func handleAuthSignUp(ctx iris.Context) {

}

func handleProfileGet(ctx iris.Context) {
	var err error
	token := ctx.GetHeader("Authorization")
	var pl jwt.Payload
	if _, err = jwt.Verify([]byte(token), hs, &pl, jwt.ValidateHeader); err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		_, _ = ctx.JSON(iris.Map{
			"statusCode": iris.StatusUnauthorized,
		})
	} else {
		_, _ = ctx.JSON(iris.Map{
			"authenticated": true,
		})
	}

}
